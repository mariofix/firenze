class GenericError(Exception):
    pass


class ProcessError(Exception):
    pass


class ConfigException(Exception):
    pass
